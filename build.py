#!/usr/bin/python3

import os.path
import datetime

f = open("templates/go.go")
template_go = f.read()
f.close()

for l in os.listdir("licenses"):
    f = open(os.path.join("licenses", l))
    s = f.read()
    f.close()

    f = open("_site/" + l + ".txt", "w")
    f.write(s)
    f.close()

    f = open("_site/ma_124/" + l + ".txt", "w")
    f.write(s
            .replace("<year>", str(datetime.datetime.now().year))
            .replace("<copyright holders>", "Ma_124 <gitlab.com/Ma_124>"))
    f.close()

    f = open("lambda/" + l + ".go", "w")
    f.write(template_go
            .replace("%license%", s)
            .replace("<year>", "` + year + `")
            .replace("<copyright holders>", "` + holders + `"))
    f.close()
